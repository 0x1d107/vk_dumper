import vk,requests,lxml.html,json,re,datetime,os,tqdm,time
import sys
from dataclasses import dataclass
from collections import defaultdict
class invalid_password(Exception): # by https://habr.com/en/post/446172/
    def __init__(self, value):self.value = value
    def __str__(self):return repr(self.value)
class not_valid_method(Exception):
    def __init__(self, value):self.value = value
    def __str__(self):return repr(self.value)

class messages(object):
    def __init__(self,login,password):
        self.login = login
        self.password = password
        self.hashes = {}
        self.auth()
    def auth(self):
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language':'ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
            'Accept-Encoding':'gzip, deflate',
            'Connection':'keep-alive',
            'DNT':'1'}
        self.session = requests.session()
        data = self.session.get('https://vk.com/', headers=headers)
        page = lxml.html.fromstring(data.content)
        form = page.forms[0]
        form.fields['email'] = self.login
        form.fields['pass'] = self.password
        response = self.session.post(form.action, data=form.form_values())
        if "onLoginDone" not in response.text: raise invalid_password("Неправильный пароль!")
        return
    def method(self,method,v=5.87,**params):
        if method not in self.hashes:
            self._get_hash(method)
        data = {'act': 'a_run_method','al': 1,
                'hash': self.hashes[method],
                'method': method,
                'param_v':v}
        for i in params:
            data["param_"+i] = params[i]
        answer = self.session.post('https://vk.com/dev',data=data)
        return json.loads(answer.text[4:])
    def _get_hash(self,method):
        html = self.session.get('https://vk.com/dev/'+method)
        hash_0 = re.findall('onclick="Dev.methodRun\(\'(.+?)\', this\);',html.text)
        if len(hash_0)==0:
            raise not_valid_method("method is not valid")
        self.hashes[method] = hash_0[0]

cache = {}
def get_name(uid):
    if uid in cache:
        return cache[uid]
    try:
        name =( api.users.get(user_ids = str(uid)))[0]
    except vk.exceptions.VkAPIError:
        try:
            name = {'first_name':api.groups.getById(group_id=-uid)[0]['name'],'last_name':"[community]"}
        except vk.exceptions.VkAPIError:
            name = {"first_name":str(uid),'last_name':"NoName"}
    cache[uid]=name
    return name
@dataclass
class Msg:
    user:str
    date:str
    text:str
    attachments:list
    forwarded:list
    @staticmethod 
    def from_json(i):
        i=defaultdict(lambda: [],i)
        name = get_name(i['from_id'])
        date = (datetime.datetime.fromtimestamp(int(i['date'])).strftime("%c"))

        return Msg(name['first_name']+" "+name['last_name'],date,i['text'],i['attachments'],i['fwd_messages'])
    @staticmethod
    def pretty_write(file,msg,indent=0):
        def out(s):
            file.write("\t"*indent+s+"\n")
        out("{} at {}:".format(msg.user,msg.date))
        out("\t{}".format(msg.text.replace('\n','\n'+'\t'*indent+'\t')))
        if len(msg.attachments):
            out("Attachments:")
            for att in msg.attachments:
                out("\t[{}]:".format(str(att['type'])))
                out("\t\t{}".format(str(json.dumps(att[att['type']],indent=1,sort_keys=True)).replace('\n','\n'+'\t'*indent+'\t\t')))
        if len(msg.forwarded):
            out("Forwarded messages:")
            for fwd in msg.forwarded:
                Msg.pretty_write(file,Msg.from_json(fwd),indent+1)


#vk.logger.setLevel(10)
with open(".secret_api_key.txt",'r') as f:
    service_token =re.sub(r"\W+",'',f.read())
    #print('Token:',service_token)
session = vk.Session(service_token)
api = vk.API(session,v='5.107')
BIG_DATA=[]
a=messages(sys.argv[1],sys.argv[2])
if sys.argv[3].startswith('c'):
    peer = 2000000000+int(sys.argv[3][1:])
else:
    peer = int(sys.argv[3])
pld = json.loads(a.method('messages.getHistory',count=200,offset=0,peer_id=peer)['payload'][1][0])['response']
mc = pld['count']
with tqdm.tqdm(total=mc) as p:
    for offset in range(0,mc,200):
        pld = json.loads(a.method('messages.getHistory',count=200,offset=offset,peer_id=peer)['payload'][1][0])['response']
#        print("Total message count:",mc,"Offset:",offset)
        data = pld['items']
        
        for i in data:
            
            name =get_name(i['from_id'])
            
            date = (datetime.datetime.fromtimestamp(int(i['date'])).strftime("%c"))
    #        print(name['first_name'],name['last_name'],date)
    #        print("\t"+i['text'])
    #        if len (i['attachments']):
    #            print("Attachments:")
    #            for att in i['attachments']:
    #                print("\t"+att['type'])
    #                print("\t\t"+str(att))
            try:
                BIG_DATA.append(Msg(name['first_name']+" "+name['last_name'],date,i['text'],i['attachments'],i['fwd_messages']))
            except Exception as e:
                print("ERROR:",e)
                print (i)
            time.sleep(0.01)
            p.update(1)
BIG_DATA.reverse()
f = open("dump.txt","w")
for d in BIG_DATA:
    Msg.pretty_write(f,d)
f.close()
