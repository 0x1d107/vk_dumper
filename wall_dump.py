import vk,sys,tqdm,json,jinja2,datetime,time



session = vk.AuthSession(app_id='7497780',user_login=sys.argv[1],user_password=sys.argv[2])
api = vk.API(session,v='5.85')
cnt = api.wall.get(owner_id=sys.argv[3])['count']
posts = api.wall.get(owner_id=sys.argv[3],count=cnt)['items']
if len (sys.argv)>4:
    tmpl_name = sys.argv[4]
else:
    tmpl_name = 'post.j2'
with open(tmpl_name) as template:
    tmpl = jinja2.Template(template.read())
cache = {}
def get_name(uid):
    if uid in cache:
        return cache[uid]
    try:
        name =( api.users.get(user_ids = uid))[0]
    except vk.exceptions.VkAPIError:
        try:
            name = {'first_name':api.groups.getById(group_id=-uid)[0]['name'],'last_name':"[community]"}
        except vk.exceptions.VkAPIError as e:
            print(e)
            name = {"first_name":str(uid),'last_name':"NoName"}
    cache[uid]=name
    return name
batch = len(posts)
with tqdm.tqdm(total = cnt) as p:
    for offset in range(0,cnt,batch):
        posts = api.wall.get(owner_id=sys.argv[3],count=cnt,offset=offset)['items']
        for post in posts:
            
            #print(json.dumps(post,indent=1,sort_keys=True))
            f_name = "{date}_{post_type}_id_{id}_from_{from_id}.txt".format_map(post)
            post['datetime']=datetime.datetime.fromtimestamp(post['date']).strftime('%c')
            owner_name=get_name(post['owner_id'])
            from_name=get_name(post['from_id'])
            post['from']="{first_name} {last_name}".format_map(from_name)
            post['owner']="{first_name} {last_name}".format_map(owner_name)
            with open(f_name,'w') as file:
                file.write(tmpl.render(post))
            p.update(1)
            time.sleep(0.05)
